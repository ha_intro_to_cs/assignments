import java.util.Scanner;
import java.util.Random;

public class GuessingGame {
    public static void main(String[] args) {
        System.out.println("Welcome to the guessing game! I am thinking of a "
            + "number between 1 and 100.");
        System.out.println("Try to guess it, and I will help you by telling "
            + "you higher or lower!");
        System.out.println();

        Random rng = new Random();
        int number = rng.nextInt(100) + 1;

        Scanner inputScanner = new Scanner(System.in);

        int numberOfGuesses = 0;

        while (true) {
            System.out.print("Enter your guess: ");
            String input = inputScanner.nextLine();
            int guess;
            try {
                guess = Integer.parseInt(input);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid input: " + input);
                continue;
            }

            numberOfGuesses++;

            if (guess == number) {
                System.out.println("Correct!");
                System.out.println("It took you " + numberOfGuesses + " guesses.");
                break;
            }
            else if (guess < number) {
                System.out.println("Too low!");
            }
            else {
                System.out.println("Too high!");
            }
        }
    }
}
