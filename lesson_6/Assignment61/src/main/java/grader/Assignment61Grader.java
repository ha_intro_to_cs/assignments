package grader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Assignment61Grader {
    private static final Logger logger = LogManager.getLogger(Assignment61Grader.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("usage: java -jar Assignment61Grader.jar <assignment_directory>");
            return;
        }

        logger.info("Beginning grading");

        Path compileDirectory = Paths.get(args[0]);

        TestRunner runner = new TestRunner(compileDirectory.toFile(), Arrays.asList("javac", "Guess.java"));

        ProgramRunner programRunner = new ProgramRunner(compileDirectory.toFile(), Arrays.asList("java", "Guess"));

        // Seed to make the program generate "5".
        int seed = 18445;

        String welcomeText = "I am thinking of a number between 1 and 10. Try to guess it!";
        List<SimpleInteractiveTestCase.ExpectedIO> expectedIO;

        runner.addTest(new SimpleInteractiveTestCase(
                "Unknown Seed", 10, programRunner, Collections.emptyList(),
                welcomeText, Collections.emptyList()));

        TestCategory knownSeed = new TestCategory("Known Seed");

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("5", "You got it!"));
        knownSeed.addTest(new SimpleInteractiveTestCase(
                "Correct Guess", 10, programRunner, Arrays.asList(String.valueOf(seed)),
                welcomeText, expectedIO
        ));

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("7", "Too high!"));
        knownSeed.addTest(new SimpleInteractiveTestCase(
                "High Guess", 10, programRunner, Arrays.asList(String.valueOf(seed)),
                welcomeText, expectedIO
        ));

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("3", "Too low!"));
        knownSeed.addTest(new SimpleInteractiveTestCase(
                "Low Guess", 10, programRunner, Arrays.asList(String.valueOf(seed)),
                welcomeText, expectedIO
        ));

        runner.addTest(knownSeed);

        TestCategory errors = new TestCategory("Errors");

        errors.addTest(new SimpleTestCase("Invalid seed", 10, programRunner,
                Arrays.asList("garbage"), "Invalid seed: garbage"));

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("seventy-two", "Invalid integer: seventy-two"));
        errors.addTest(new SimpleInteractiveTestCase(
                "Invalid Guess", 10, programRunner, Arrays.asList(String.valueOf(seed)),
                welcomeText, expectedIO
        ));

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("🤡", "Invalid integer: 🤡"));
        errors.addTest(new SimpleInteractiveTestCase(
                "Very Invalid Guess", 10, programRunner, Arrays.asList(String.valueOf(seed)),
                welcomeText, expectedIO
        ));

        runner.addTest(errors);

        runner.runTests();
    }
}
