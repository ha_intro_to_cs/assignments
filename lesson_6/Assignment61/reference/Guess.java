import java.util.Random;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Guess {
    public static void main(String[] args) {
        Random rng;
        if (args.length < 1) {
            rng = new Random();
        }
        else {
            int seed;
            try {
                seed = Integer.parseInt(args[0]);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid seed: " + args[0]);
                return;
            }
            rng = new Random(seed);
        }
        int number = rng.nextInt(10) + 1;

        System.out.println("I am thinking of a number between 1 and 10. Try to guess it!");

        System.out.print("> ");

        Scanner in = new Scanner(System.in);
        int guess;
        try {
            guess = in.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid integer: " + in.nextLine());
            return;
        }

        if (guess == number) {
            System.out.println("You got it!");
        }
        else if (guess < number) {
            System.out.println("Too low!");
        }
        else {
            System.out.println("Too high!");
        }
    }
}