public class BinDec {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: BinDec <base> <number>");
            return;
        }
        String base = args[0];
        String number = args[1];

        if (base.equals("-b")) {
            try {
                System.out.println(Integer.parseInt(number, 2));
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid number: " + number);
            }

        }
        else if (base.equals("-d")) {
            try {
                System.out.println(Integer.toBinaryString(Integer.parseInt(number)));
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid number: " + number);
            }
        }
        else {
            System.out.println("Unsupported option for base: " + base);
        }
    }
}