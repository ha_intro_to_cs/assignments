package grader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

public class Assignment62Grader {
    private static final Logger logger = LogManager.getLogger(Assignment62Grader.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("usage: java -jar grader.Assignment62Grader.jar <assignment_directory>");
            return;
        }

        logger.info("Beginning grading");

        Path compileDirectory = Paths.get(args[0]);

        TestRunner runner = new TestRunner(compileDirectory.toFile(), Arrays.asList("javac", "BinDec.java"));

        ProgramRunner programRunner = new ProgramRunner(compileDirectory.toFile(), Arrays.asList("java", "BinDec"));

        TestCategory bin = new TestCategory("Binary input");
        bin.addTest(new SimpleTestCase("0b0", 10, programRunner, Arrays.asList("-b", "0"), "0"));
        bin.addTest(new SimpleTestCase("0b11111111", 10, programRunner, Arrays.asList("-b", "11111111"), "255"));
        bin.addTest(new SimpleTestCase("0b10101010", 10, programRunner, Arrays.asList("-b", "10101010"), "170"));
        bin.addTest(new SimpleTestCase("0b01101110", 10, programRunner, Arrays.asList("-b", "01101110"), "110"));
        bin.addTest(new SimpleTestCase("0b1001011010110110", 10, programRunner,
                Arrays.asList("-b", "1001011010110110"), "38582"));
        runner.addTest(bin);

        TestCategory dec = new TestCategory("Decimal input");
        dec.addTest(new SimpleTestCase("0d0", 10, programRunner, Arrays.asList("-d", "0"), "0"));
        dec.addTest(new SimpleTestCase("0d255", 10, programRunner, Arrays.asList("-d", "255"), "11111111"));
        dec.addTest(new SimpleTestCase("0d117", 10, programRunner, Arrays.asList("-d", "117"), "1110101"));
        dec.addTest(new SimpleTestCase("0d1010", 10, programRunner, Arrays.asList("-d", "1010"), "1111110010"));
        dec.addTest(new SimpleTestCase("0d157901247", 10, programRunner,
                Arrays.asList("-d", "157901247"), "1001011010010110000110111111"));
        runner.addTest(dec);

        TestCategory errors = new TestCategory("Errors");
        errors.addTest(new SimpleTestCase("No args", 10, programRunner,
                Collections.emptyList(), "Usage: BinDec <base> <number>"));
        errors.addTest(new SimpleTestCase("One arg", 10, programRunner,
                Arrays.asList("-d"), "Usage: BinDec <base> <number>"));
        errors.addTest(new SimpleTestCase("Invalid base", 10, programRunner,
                Arrays.asList("-o", "31"), "Unsupported option for base: -o"));
        errors.addTest(new SimpleTestCase("Outright wrong bin", 10, programRunner,
                Arrays.asList("-b", "garbo"), "Invalid number: garbo"));
        errors.addTest(new SimpleTestCase("Outright wrong dec", 10, programRunner,
                Arrays.asList("-d", "garbo"), "Invalid number: garbo"));
        errors.addTest(new SimpleTestCase("Wrong bin", 10, programRunner,
                Arrays.asList("-b", "123"), "Invalid number: 123"));
        runner.addTest(errors);

        runner.runTests();
    }
}
