public class Psalm1 {
    public static void main(String[] args) {
        System.out.println("1 How blessed is the man who does not walk in the counsel of the wicked,\n" +
                "Nor stand in the path of sinners,\n" +
                "Nor sit in the seat of scoffers!\n" +
                "\n" +
                "2 But his delight is in the law of the Lord,\n" +
                "And in His law he meditates day and night.\n" +
                "\n" +
                "3 He will be like a tree firmly planted by streams of water,\n" +
                "Which yields its fruit in its season\n" +
                "And its leaf does not wither;\n" +
                "And in whatever he does, he prospers.\n" +
                "\n" +
                "4 The wicked are not so,\n" +
                "But they are like chaff which the wind drives away.\n" +
                "\n" +
                "5 Therefore the wicked will not stand in the judgment,\n" +
                "Nor sinners in the assembly of the righteous.\n" +
                "\n" +
                "6 For the Lord knows the way of the righteous,\n" +
                "But the way of the wicked will perish.");
    }
}
