package grader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Assignment20Grader {
    private static Logger logger = LogManager.getLogger(Assignment20Grader.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("usage: java -jar Assignment20Grader.jar <assignment_directory>");
            return;
        }

        logger.info("Beginning grading");

        Path compileDirectory = Paths.get(args[0]);

        TestRunner runner = new TestRunner(compileDirectory.toFile(), Arrays.asList("javac", "Psalm1.java"));

        ProgramRunner programRunner = new ProgramRunner(compileDirectory.toFile(), Arrays.asList("java", "Psalm1"));

        String psalm1 = "1 How blessed is the man who does not walk in the counsel of the wicked," + System.lineSeparator() +
                "Nor stand in the path of sinners," + System.lineSeparator() +
                "Nor sit in the seat of scoffers!" + System.lineSeparator() +
                "" + System.lineSeparator() +
                "2 But his delight is in the law of the Lord," + System.lineSeparator() +
                "And in His law he meditates day and night." + System.lineSeparator() +
                "" + System.lineSeparator() +
                "3 He will be like a tree firmly planted by streams of water," + System.lineSeparator() +
                "Which yields its fruit in its season" + System.lineSeparator() +
                "And its leaf does not wither;" + System.lineSeparator() +
                "And in whatever he does, he prospers." + System.lineSeparator() +
                "" + System.lineSeparator() +
                "4 The wicked are not so," + System.lineSeparator() +
                "But they are like chaff which the wind drives away." + System.lineSeparator() +
                "" + System.lineSeparator() +
                "5 Therefore the wicked will not stand in the judgment," + System.lineSeparator() +
                "Nor sinners in the assembly of the righteous." + System.lineSeparator() +
                "" + System.lineSeparator() +
                "6 For the Lord knows the way of the righteous," + System.lineSeparator() +
                "But the way of the wicked will perish.";

        SimpleTestCase output = new SimpleTestCase("Output", 100, programRunner, Collections.emptyList(), psalm1);

        runner.addTest(output);

        runner.runTests();

        // Add logs, and figure out how to show expected vs actual. Maybe just use hint?
    }
}
