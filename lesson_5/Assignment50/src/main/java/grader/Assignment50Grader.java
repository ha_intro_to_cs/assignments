package grader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Assignment50Grader {
    private static final Logger logger = LogManager.getLogger(Assignment50Grader.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("usage: java -jar Assignment50Grader.jar <assignment_directory>");
            return;
        }

        logger.info("Beginning grading");

        Path compileDirectory = Paths.get(args[0]);

        TestRunner runner = new TestRunner(compileDirectory.toFile(), Arrays.asList("javac", "TipCalculator.java"));

        ProgramRunner programRunner = new ProgramRunner(compileDirectory.toFile(), Arrays.asList("java", "TipCalculator"));

        TestCategory simple = new TestCategory("Simple");
        SimpleTestCase zero = new SimpleTestCase("25.89/0%", 10, programRunner, Arrays.asList("25.89", "0"), "25.89");
        SimpleTestCase tenTen = new SimpleTestCase("$10/10%", 10, programRunner, Arrays.asList("10.00", "10"), "11.0");
        SimpleTestCase twentyThree = new SimpleTestCase("$23.48/20%", 10, programRunner, Arrays.asList("23.48", "20"), "28.176");
        SimpleTestCase fifteen = new SimpleTestCase("$15.15/15%", 10, programRunner, Arrays.asList("15.15", "15"), "17.4225");
        simple.addTest(zero);
        simple.addTest(tenTen);
        simple.addTest(twentyThree);
        simple.addTest(fifteen);
        runner.addTest(simple);

        TestCategory errors = new TestCategory("Errors");
        SimpleTestCase fewArgs = new SimpleTestCase("Too Few Args", 10, programRunner, Arrays.asList("10"), "");
        SimpleTestCase nanBill = new SimpleTestCase("NaN Bill", 10, programRunner, Arrays.asList("garbo", "10"), "");
        SimpleTestCase nanTip = new SimpleTestCase("NaN Tip", 10, programRunner, Arrays.asList("10", "garbo"), "");
        errors.addTest(fewArgs);
        errors.addTest(nanBill);
        errors.addTest(nanTip);
        runner.addTest(errors);

        TestCategory outlandish = new TestCategory("Outlandish");
        SimpleTestCase negative = new SimpleTestCase("$20.45/-15%", 5, programRunner, Arrays.asList("20.45", "-15"), "17.3825");
        SimpleTestCase over100 = new SimpleTestCase("$4.82/200%", 5, programRunner, Arrays.asList("4.82", "200"), "14.46");
        outlandish.addTest(negative);
        outlandish.addTest(over100);
        runner.addTest(outlandish);

        runner.runTests();
    }
}
