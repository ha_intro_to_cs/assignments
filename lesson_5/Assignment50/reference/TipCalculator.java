public class TipCalculator {
    public static void main(String[] args) {
        double billAmount = Double.parseDouble(args[0]);
        int tipPercentInt = Integer.parseInt(args[1]);
        double tipPercent = 1 + tipPercentInt / 100.0;
        double totalAmount = tipPercent * billAmount;

        System.out.println(totalAmount);
    }
}