import java.util.Scanner;

public class MadLibs {
    public static void main(String[] args) {
        System.out.println("Welcome to Mad Libs! To play this game supply a word of the type required by");
        System.out.println("each prompt. After filling in all the blanks, a story using your input will be");
        System.out.println("generated!");
        System.out.println();

        Scanner in = new Scanner(System.in);

        System.out.print("Adjective > ");
        String adj1 = in.nextLine();

        System.out.print("Adjective > ");
        String adj2 = in.nextLine();

        System.out.print("Noun > ");
        String noun1 = in.nextLine();

        System.out.print("Noun > ");
        String noun2 = in.nextLine();

        System.out.print("Plural Noun > ");
        String pNoun1 = in.nextLine();

        System.out.print("Game > ");
        String game1 = in.nextLine();

        System.out.print("Plural Noun > ");
        String pNoun2 = in.nextLine();

        System.out.print("Verb ending in 'ing' > ");
        String verbing1 = in.nextLine();

        System.out.print("Verb ending in 'ing' > ");
        String verbing2 = in.nextLine();

        System.out.print("Plural Noun > ");
        String pNoun3 = in.nextLine();

        System.out.print("Verb ending in 'ing' > ");
        String verbing3 = in.nextLine();

        System.out.print("Noun > ");
        String noun3 = in.nextLine();

        System.out.print("Plant > ");
        String plant1 = in.nextLine();

        System.out.print("Part of the body > ");
        String body1 = in.nextLine();

        System.out.print("A place > ");
        String place1 = in.nextLine();

        System.out.print("Verb ending in 'ing' > ");
        String verbing4 = in.nextLine();

        System.out.print("Adjective > ");
        String adj3 = in.nextLine();

        System.out.print("Number > ");
        double number1 = in.nextDouble();
        in.nextLine();

        System.out.print("Plural Noun > ");
        String pNoun4 = in.nextLine();

        System.out.println();

        System.out.println("A vacation is when you take a trip to some " + adj1 + " place");
        System.out.println("with your " + adj2 + " family. Usually you go to some place");
        System.out.println("that is near a/an " + noun1 + " or up on a/an " + noun2 + ".");
        System.out.println("A good vacation place is one where you can ride " + pNoun1);
        System.out.println("or play " + game1 + " or go hunting for " + pNoun2 + ". I like");
        System.out.println("to spend my time " + verbing1 + " or " + verbing2 + ".");
        System.out.println("When parents go on a vacation, they spend their time eating");
        System.out.println("three " + pNoun3 + " a day, and fathers play golf, and mothers");
        System.out.println("sit around " + verbing3 + ". Last summer, my little brother");
        System.out.println("fell in a/an " + noun3 + " and got poison " + plant1 + " all");
        System.out.println("over his " + body1 + ". My family is going to go to (the)");
        System.out.println(place1 + ", and I will practice " + verbing4 + ". Parents");
        System.out.println("need vacations more than kids because parents are always very");
        System.out.println(adj3 + ", and because they have to work " + number1);
        System.out.println("hours every day all year making enough " + pNoun4 + " to pay");
        System.out.println("for the vacation.");

    }
}