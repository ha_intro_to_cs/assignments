package grader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Assignment51Grader {
    private static final Logger logger = LogManager.getLogger(Assignment51Grader.class);
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("usage: java -jar Assignment51Grader.jar <assignment_directory>");
            return;
        }

        logger.info("Beginning grading");

        Path compileDirectory = Paths.get(args[0]);

        TestRunner runner = new TestRunner(compileDirectory.toFile(), Arrays.asList("javac", "MadLibs.java"));

        ProgramRunner programRunner = new ProgramRunner(compileDirectory.toFile(), Arrays.asList("java", "MadLibs"));

        String initialExpectedOutput = "Welcome to Mad Libs! To play this game supply a word of the type required by\n" +
                "each prompt. After filling in all the blanks, a story using your input will be\n" +
                "generated!\n" +
                "\n" +
                "Adjective";

        List<SimpleInteractiveTestCase.ExpectedIO> expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("1", "Adjective"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("2", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("3", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("4", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("5", "Game"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("6", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("7", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("8", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("9", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("10", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("11", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("12", "Plant"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("13", "Part of the body"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("14", "A place"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("15", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("16", "Adjective"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("17", "Number"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("18", "Plural Noun"));

        String story = "A vacation is when you take a trip to some 1 place" + System.lineSeparator() +
                "with your 2 family. Usually you go to some place" + System.lineSeparator() +
                "that is near a/an 3 or up on a/an 4." + System.lineSeparator() +
                "A good vacation place is one where you can ride 5" + System.lineSeparator() +
                "or play 6 or go hunting for 7. I like" + System.lineSeparator() +
                "to spend my time 8 or 9." + System.lineSeparator() +
                "When parents go on a vacation, they spend their time eating" + System.lineSeparator() +
                "three 10 a day, and fathers play golf, and mothers" + System.lineSeparator() +
                "sit around 11. Last summer, my little brother" + System.lineSeparator() +
                "fell in a/an 12 and got poison 13 all" + System.lineSeparator() +
                "over his 14. My family is going to go to (the)" + System.lineSeparator() +
                "15, and I will practice 16. Parents" + System.lineSeparator() +
                "need vacations more than kids because parents are always very" + System.lineSeparator() +
                "17, and because they have to work 18.0" + System.lineSeparator() +
                "hours every day all year making enough 19 to pay" + System.lineSeparator() +
                "for the vacation.";

        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("19", story));

        SimpleInteractiveTestCase output = new SimpleInteractiveTestCase("Output", 10, programRunner, Collections.emptyList(), initialExpectedOutput, expectedIO);
        runner.addTest(output);

        expectedIO = new ArrayList<>();
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("1", "Adjective"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("2", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("3", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("4", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("5", "Game"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("6", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("7", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("8", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("9", "Plural Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("10", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("11", "Noun"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("12", "Plant"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("13", "Part of the body"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("14", "A place"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("15", "Verb ending in 'ing'"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("16", "Adjective"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("17", "Number"));
        expectedIO.add(new SimpleInteractiveTestCase.ExpectedIO("garbo", ""));
        SimpleInteractiveTestCase number = new SimpleInteractiveTestCase("Number", 10, programRunner, Collections.emptyList(), initialExpectedOutput, expectedIO);
        runner.addTest(number);

        runner.runTests();

    }
}
